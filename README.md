# edencreeper's build of dwm

## Dependencies
### Stuff you will _definitely_ need
- My build of slstatus
- My build of dmenu (or, well, any buid of dmenu _should_ work.)
- Kitty
### Other stuff I have set for stuff
- DT's dm-scripts, as I use a few for certain stuffs
- qutebrowser
- emacs
- lightcord
- spotify
- pactl
- xkill
- music-controller found in my dotfiles repo
- popinfo found in my dotfiles repo (or HexDSLs one)
- nitrogen
- lxsession
- dunst
- my build of slock
- picom-ibhagwan


## Installation
Clone the repo with git `git clone https://github.com/edencreeper/dwm`

After entering into the repo, run `sudo make clean install` (or use the dmake alias from my dotfiles ig)

### Branches being weird
This is much more so i dont forget this, but sometimes all the branches dont track from remote. You may have to do this to fix it
```
git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done\
```

## Running
I start it from xinitrc. If you wanna do it from a login manager, make a file for it, but thats not what im doing here.

In the .xinitrc, you will _at least_ need `exec dwm`. I prefer to use `while true;  do dwm > /dev/null; done;` in mine, to enable the posibility to restart the wm.

Below is my full .xinitrc
```
nitrogen --restore &
lxsession &
xrdb -merge $HOME/.Xresources &
setxkbmap gb -option caps:swapescape &
dunst &
slstatus &
picom &
while true;  do dwm > /dev/null; done;
```


## Using
### Keybinds
This is not a definitive list, but should be enough to get you started. Read the config.def.h for other stuff
`M` represents the mod key. I have it set to `Mod4Mask` (the windows key)
| Keybind        | What It Does                 |
| -------------- | ---------------------------- | 
| M-Return       | Kitty (terminal emulator)    |
| M-Shift-Return | Dmenu (Application Launcher) |
| M-w            | Qutebrowser (Browser)        |
| M-{number key} | Change Workspace             |

### TODO Patching
uhmmm. it says `TODO`. leave me alone im lazy
